package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Day_part struct {
	Typeid           int    `xml:"typeid,attr"`
	Type             string `xml:"type,attr"`
	Temperature_from string `xml:"temperature_from"`
}

type Fact struct {
	Temperature        string `xml:"temperature"`
	Weather_type_short string `xml:"weather_type"`
}

type Day struct {
	Date     string     `xml:"date,attr"`
	Day_part []Day_part `xml:"day_part"`
}
type Result struct {
	XMLName xml.Name `xml:"forecast"`
	City    string   `xml:"city,attr"`
	Fact    []Fact   `xml:"fact"`
	Day     []Day    `xml:"day"`
}

func main() {
	v := Result{}
	res, ErrRes := http.Get("http://export.yandex.ru/weather-ng/forecasts/34214.xml")

	if ErrRes != nil {
		fmt.Printf("File error: %v\n", ErrRes)
		panic(ErrRes)
	}

	xmlContent, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()
	err := xml.Unmarshal(xmlContent, &v)
	if err != nil {
		fmt.Println("error")
		panic(err)
	}
	fmt.Println("Город", v.City)
	for _, day := range v.Day {
		for _, dayPart := range day.Day_part {
			if dayPart.Typeid == 1 {
				morningTemp := make(map[string]string)
				morningTemp["Утро"] = dayPart.Temperature_from
				for key, value := range morningTemp {
					//	fmt.Println(key, "-", value)
					fmt.Printf(" %s %s ", key, value)
				}
				//fmt.Printf("%s  %s\n", day.Date, dayPart.Temperature_from)
			} else if dayPart.Typeid == 2 {
				dayTemp := make(map[string]string)
				dayTemp["День"] = dayPart.Temperature_from
				for key, value := range dayTemp {
					fmt.Printf("%s  %s\n ", key, value)
				}
			}
		}
	}
	/*	for _, fact := range v.Fact {
			fmt.Println("Город:", v.City)
			fmt.Println("Температура:", fact.Temperature, "\nПогода:", fact.Weather_type_short)
		}
	*/
}
