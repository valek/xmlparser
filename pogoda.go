package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Forecast struct {
	Temperature        string `xml:"temperature"`
	Weather_type_short string `xml:"weather_type_short"`
}

type Forecasts struct {
	City      string     `xml:"city,attr"`
	Forecasts []Forecast `xml:"fact"`
}

func main() {

	p := Forecasts{}
	pog, _ := http.Get("http://export.yandex.ru/weather-ng/forecasts/34214.xml")
	xmlPogoda, _ := ioutil.ReadAll(pog.Body)
	err_pog := xml.Unmarshal(xmlPogoda, &p)
	if err_pog != nil {
		fmt.Println("ERROR!")
		panic(err_pog)
	}
	for _, fact := range p.Forecasts {
		fmt.Println("Город:", p.City)
		fmt.Println("Температура:", fact.Temperature, "\nПогода:", fact.Weather_type_short)
	}
}
