package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
)

//  Создаем ключи для консольной программы
var (
	flStrana = flag.String("c", "", "Страна для которой хотите получить список городов. Например Россия")
	flAll    = flag.String("list", "", "Список всех городов -list=`all`")
	fGorod   = flag.String("g", "", "Поиск по городу")

	id_gorod string
)

// Описываем структуру XML файла, который будем парсить
type cities struct {
	XMLName xml.Name `xml:"cities"`
	Country country
}

type country struct {
	XMLName xml.Name `xml:"country"`
	Name    string   `xml:"name,attr"`
	City    []city   `xml:"city"`
}

type city struct {
	Id      string `xml:"id,attr"`
	Region  string `xml:"region,attr"`
	Head    string `xml:"head,attr"`
	Type    string `xml:"type,attr"`
	Country string `xml:"country,attr"`
	Part    string `xml:"part,attr"`
	Resort  string `xml:"resort,attr"`
	Climate string `xml:"climate,attr"`
	Content string `xml:",chardata"`
}

type Forecast struct {
	Temperature        string `xml:"temperature"`
	Weather_type_short string `xml:"weather_type_short"`
}

type Forecasts struct {
	City      string     `xml:"city,attr"`
	Forecasts []Forecast `xml:"fact"`
}

func main() {
	flag.Parse()
	d := cities{}
	res, _ := http.Get("http://weather.yandex.ru/static/cities.xml")
	xmlContent, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()
	err := xml.Unmarshal(xmlContent, &d)
	if err != nil {
		fmt.Println("error")
		panic(err)
	}
	for _, city := range d.Country.City {
		if city.Content == *fGorod {
			fmt.Println("Id города:", city.Id, "Город:", city.Content)

			url_template := `http://export.yandex.ru/weather-ng/forecasts/%s.xml`
			url := fmt.Sprintf(url_template, city.Id)
			fmt.Println(url)

			p := Forecasts{}
			pog, _ := http.Get(url)
			xmlPogoda, _ := ioutil.ReadAll(pog.Body)
			pog.Body.Close()

			err_pog := xml.Unmarshal(xmlPogoda, &p)
			if err_pog != nil {
				fmt.Println("ERROR!")
				panic(err_pog)
			}
			for _, fact := range p.Forecasts {
				fmt.Println("Город:", p.City)
				fmt.Println("Температура:", fact.Temperature, "\nПогода:", fact.Weather_type_short)
			}

		}
		if city.Country == *flStrana {
			fmt.Println("Id города:", city.Id, "Город:", city.Content)
		}
		if *flAll == "all" {
			fmt.Println("Город:", city.Content, "Страна:", city.Country, "ID погоды:", city.Id)
		}
	}
}
